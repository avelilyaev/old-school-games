﻿using System;
using System.Timers;

namespace Tetris
{
    class Program
    {
        static void SetCustomConsole()
        {
            Console.Title = "Tetris";
            Console.ForegroundColor = ConsoleColor.Green;
            Console.CursorVisible = false;
            Console.WindowWidth = 25;
            Console.WindowHeight = 22;
        }

        static void Main(string[] args)
        {
            SetCustomConsole();
            ConsoleKeyInfo Key;
            GameManager.Instance.StartGame();
            do
            {
                Key = Console.ReadKey();
                GameManager.Instance.ButtonClickHandler(Key.Key);
            }
            while (Key.Key != ConsoleKey.Escape);
        }
    }
}
