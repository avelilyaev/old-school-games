﻿namespace Tetris
{
    public static class Field
    {
        internal const int WIDTH = 10;
        internal const int HEIGHT = 20;

        internal static bool[,] Matrix { get { return (bool[,])_matrix.Clone(); } }
        internal static bool[,] NextMatrix { get { return (bool[,])_nextTetraminoMatrix.Clone(); } }

        internal static void Put(Tetramino tetramino, bool value)
        {
            _matrix[tetramino.Cells[0].Y, tetramino.Cells[0].X] = value;
            _matrix[tetramino.Cells[1].Y, tetramino.Cells[1].X] = value;
            _matrix[tetramino.Cells[2].Y, tetramino.Cells[2].X] = value;
            _matrix[tetramino.Cells[3].Y, tetramino.Cells[3].X] = value;
        }

        internal static void SetNextTetramino(Tetramino nextTetramino)
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    _nextTetraminoMatrix[i, j] = false;
                }
            }
            _nextTetraminoMatrix[nextTetramino.Cells[0].Y, nextTetramino.Cells[0].X-3] = true;
            _nextTetraminoMatrix[nextTetramino.Cells[1].Y, nextTetramino.Cells[1].X-3] = true;
            _nextTetraminoMatrix[nextTetramino.Cells[2].Y, nextTetramino.Cells[2].X-3] = true;
            _nextTetraminoMatrix[nextTetramino.Cells[3].Y, nextTetramino.Cells[3].X-3] = true;
        }

        internal static int CheckFullLines()
        {
            int score = 0;
            for (int i = 0; i < HEIGHT; i++)
            {
                bool isFullLine = true;

                for (int j = 0; j < WIDTH; j++)
                {
                    isFullLine = isFullLine && _matrix[i, j];
                }

                if (isFullLine)
                {
                    ShiftMatrix(i);
                    score += 10;
                }
            }
            return score;
        }

        private static void ShiftMatrix(int rowNumber)
        {
            for (int i = rowNumber; i > 0; i--)
            {
                for (int j = 0; j < WIDTH; j++)
                {
                    _matrix[i, j] = _matrix[i - 1, j];
                    _matrix[i - 1, j] = false;
                }
            }
        }

        internal static void Reset()
        {
            for (int i = 0; i < HEIGHT; i++)
            {
                for (int j = 0; j < WIDTH; j++)
                {
                    _matrix[i, j] = false;
                }
            }
        }

        private static bool[,] _matrix = new bool[HEIGHT, WIDTH];
        private static bool[,] _nextTetraminoMatrix = new bool[4, 4];
    }
}
