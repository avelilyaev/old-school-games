﻿namespace Tetris
{
    public enum TetraState { First, Second, Third, Fourth }

    public class State
    {
        private int[] _states = new int[] { 0, 1, 2, 3 };
        private int _current = 0;

        public TetraState Current
        {
            get { return (TetraState)_states[_current]; }
        }

        public void Switch()
        {
            _current = _current == 3 ? 0 : _current + 1;
        }

        public TetraState NextState
        {
            get { return (TetraState)(_current == 3 ? 0 : _current + 1); }
        }
    }
}
