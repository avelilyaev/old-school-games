﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace Tetris
{
    public abstract class Tetramino
    {
        public Point[] Cells { get; set; }

        public int[] LeftSet { get; set; }
        public int[] RightSet { get; set; }
        public int[] DownSet { get; set; }

        protected State State { get; set; }

        public void Move(int x, int y)
        {
            for (int i = 0; i < Cells.Length; i++)
            {
                Cells[i].X += x;
                Cells[i].Y += y;
            }
        }

        public bool CanMoveDown()
        {
            foreach (int i in DownSet)
            {
                if (!(Cells[i].Y < Field.HEIGHT - 1 && !Field.Matrix[Cells[i].Y + 1, Cells[i].X]))
                    return false;
            }
            return true;
        }

        public bool CanMoveLeft()
        {
            foreach (int i in LeftSet)
            {
                if (!(Cells[i].X > 0 && !Field.Matrix[Cells[i].Y, Cells[i].X - 1]))
                    return false;
            }
            return true;
        }

        public bool CanMoveRight()
        {
            foreach (int i in RightSet)
            {
                if (!(Cells[i].X < Field.WIDTH - 1 && !Field.Matrix[Cells[i].Y, Cells[i].X + 1]))
                    return false;
            }
            return true;
        }

        protected abstract void SetOrientation(TetraState state);

        protected virtual Point[] GetShapeByState(int x, int y, TetraState state)
        {
            return null;
        }

        public virtual bool CanRotate()
        {
            Point[] testCells = GetShapeByState(Cells[0].X, Cells[0].Y, State.NextState);

            foreach (Point point in testCells)
            {
                if (point.X >= 0 && point.X < Field.WIDTH && point.Y >= 0 && point.Y < Field.HEIGHT)
                {
                    if (!new List<Point>(Cells).Contains(point))
                    {
                        if (Field.Matrix[point.Y, point.X])
                            return false;
                    }
                }
                else return false;
            }

            return true;
        }

        public void Rotate()
        {
            State.Switch();
            Cells = GetShapeByState(Cells[0].X, Cells[0].Y, State.Current);
            SetOrientation(State.Current);
        }
    }
}
