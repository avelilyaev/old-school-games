﻿using System;
using System.Drawing;

namespace Tetris
{
    /// <summary>
    /// 0    
    /// 1 => 0 1 2 3
    /// 2
    /// 3 
    /// </summary>
    public class I : Tetramino
    {
        public I(int x, int y)
        {
            Cells = new Point[] { new Point(x, y), new Point(x, y + 1), new Point(x, y + 2), new Point(x, y + 3) };
            State = new State();
            SetOrientation(State.Current);
        }

        protected override Point[] GetShapeByState(int x, int y, TetraState state)
        {
            switch (state)
            {
                case TetraState.First:
                case TetraState.Third: return new Point[] { new Point(x, y - 1), new Point(x, y), new Point(x, y + 1), new Point(x, y + 2) };
                case TetraState.Second:
                case TetraState.Fourth: return new Point[] { new Point(x, y + 1), new Point(x + 1, y + 1), new Point(x + 2, y + 1), new Point(x + 3, y + 1) };
            }
            return new Point[] { };
        }

        protected override void SetOrientation(TetraState state)
        {
            switch (state)
            {
                case TetraState.First:
                case TetraState.Third:
                    {
                        LeftSet = new int[] { 0, 1, 2, 3 };
                        RightSet = new int[] { 0, 1, 2, 3 };
                        DownSet = new int[] { 3 };
                        break;
                    }
                case TetraState.Second:
                case TetraState.Fourth:
                    {
                        LeftSet = new int[] { 0 };
                        RightSet = new int[] { 3 };
                        DownSet = new int[] { 0, 1, 2, 3 };
                        break;
                    }
            }
        }
    }
}
