﻿using System;
using System.Drawing;

namespace Tetris
{
    /// <summary>
    ///   0 1    0
    /// 2 3  =>  1 2
    ///            3
    /// </summary>
    class S : Tetramino
    {
        public S(int x, int y)
        {
            Cells = new Point[] { new Point(x, y), new Point(x + 1, y), new Point(x - 1, y + 1), new Point(x, y + 1) };
            State = new State();
            SetOrientation(State.Current);
        }

        protected override Point[] GetShapeByState(int x, int y, TetraState state)
        {
            switch (state)
            {
                case TetraState.First:
                case TetraState.Third: return new Point[] { new Point(x + 1, y), new Point(x + 2, y), new Point(x, y + 1), new Point(x + 1, y + 1) };
                case TetraState.Second:
                case TetraState.Fourth: return new Point[] { new Point(x - 1, y), new Point(x - 1, y + 1), new Point(x, y + 1), new Point(x, y + 2) };
            }
            return new Point[] { };
        }

        protected override void SetOrientation(TetraState state)
        {
            switch (state)
            {
                case TetraState.First:
                case TetraState.Third:
                    {
                        LeftSet = new int[] { 0, 2 };
                        RightSet = new int[] { 1, 3 };
                        DownSet = new int[] { 1, 2, 3 };
                        break;
                    }

                case TetraState.Second:
                case TetraState.Fourth:
                    {
                        LeftSet = new int[] { 0, 1, 3 };
                        RightSet = new int[] { 0, 2, 3 };
                        DownSet = new int[] { 1, 3 };
                        break;
                    }
            }
        }
    }
}
