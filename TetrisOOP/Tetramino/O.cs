﻿using System;
using System.Drawing;

namespace Tetris
{
    /// <summary>
    /// 0 1
    /// 2 3
    /// </summary>
    class O : Tetramino
    {
        public O(int x, int y)
        {
            Cells = new Point[] { new Point(x, y), new Point(x + 1, y), new Point(x, y + 1), new Point(x + 1, y + 1) };
            State = new State();
            SetOrientation(State.Current);
        }

        public override bool CanRotate()
        {
            return false;
        }

        protected override void SetOrientation(TetraState state)
        {
            LeftSet = new int[] { 0, 2 };
            RightSet = new int[] { 1, 3 };
            DownSet = new int[] { 2, 3 };
        }
    }
}
