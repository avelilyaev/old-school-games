﻿using System;
using System.Drawing;

namespace Tetris
{
    /// <summary>
    ///  0    3        1 0  
    ///  1 => 2 1 0 => 2  => 0 1 2
    ///3 2             3         3
    /// </summary>
    public class J : Tetramino
    {
        public J(int x, int y)
        {
            Cells = new Point[] { new Point(x, y), new Point(x, y + 1), new Point(x, y + 2), new Point(x - 1, y + 2) };
            State = new State();
            SetOrientation(State.Current);
        }

        protected override Point[] GetShapeByState(int x, int y, TetraState state)
        {
            switch (state)
            {
                case TetraState.First: return new Point[] { new Point(x + 1, y - 1), new Point(x + 1, y), new Point(x + 1, y + 1), new Point(x, y + 1) };
                case TetraState.Second: return new Point[] { new Point(x + 1, y + 2), new Point(x, y + 2), new Point(x - 1, y + 2), new Point(x - 1, y + 1) };
                case TetraState.Third: return new Point[] { new Point(x - 1, y - 2), new Point(x - 2, y - 2), new Point(x - 2, y - 1), new Point(x - 2, y) };
                case TetraState.Fourth: return new Point[] { new Point(x - 1, y + 1), new Point(x, y + 1), new Point(x + 1, y + 1), new Point(x + 1, y + 2) };
            }
            return new Point[] { };
        }

        protected override void SetOrientation(TetraState state)
        {
            switch (state)
            {
                case TetraState.First:
                    {
                        LeftSet = new int[] { 0, 1, 3 };
                        RightSet = new int[] { 0, 1, 2 };
                        DownSet = new int[] { 2, 3 };
                        break;
                    }
                case TetraState.Second:
                    {
                        LeftSet = new int[] { 2, 3 };
                        RightSet = new int[] { 0, 3 };
                        DownSet = new int[] { 0, 1, 2 };
                        break;
                    }
                case TetraState.Third:
                    {
                        LeftSet = new int[] { 1, 2, 3 };
                        RightSet = new int[] { 0, 2, 3 };
                        DownSet = new int[] { 0, 3 };
                        break;
                    }
                case TetraState.Fourth:
                    {
                        LeftSet = new int[] { 0, 3 };
                        RightSet = new int[] { 2, 3 };
                        DownSet = new int[] { 0, 1, 3 };
                        break;
                    }
            }
        }
    }
}
