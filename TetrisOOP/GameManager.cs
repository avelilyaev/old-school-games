﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;

namespace Tetris
{
    public class GameManager
    {
        public int Score = 0;
        public Tetramino Tetramino;

        private GameManager() { }

        public void StartGame()
        {
            Field.Reset();
            Tetramino = GetNextTetramino();
            Field.Put(Tetramino, true);
            DisplayField();

            _timer.Elapsed += delegate
            {
                if (Tetramino.CanMoveDown())
                {
                    Move(0, 1);
                }
                else if (Tetramino.Cells[0].Y == 0)
                {
                    Field.Reset();
                    Score = 0;
                }
                else
                {
                    Score += Field.CheckFullLines();
                    Tetramino = GetNextTetramino();
                }
            };

            _timer.Start();
        }

        public void ButtonClickHandler(ConsoleKey key)
        {
            if (key == ConsoleKey.LeftArrow && Tetramino.CanMoveLeft())
            {
                Move(-1, 0);
            }
            else if (key == ConsoleKey.RightArrow && Tetramino.CanMoveRight())
            {
                Move(1, 0);
            }
            else if (key == ConsoleKey.DownArrow && Tetramino.CanMoveDown())
            {
                Move(0, 1);
            }
            else if (key == ConsoleKey.UpArrow && Tetramino.CanRotate())
            {
                Rotate();
            }
        }

        //todo: так как этот метод может вызваться из двух потоков, то может возникнуть проблема с переполнением массива - y принимает значение 20
        public void Move(int x, int y)
        {
            lock (Mutex)
            {
                Console.Clear();
                Field.Put(Tetramino, false);
                Tetramino.Move(x, y);
                Field.Put(Tetramino, true);
                DisplayField();
            }
        }

        public void Rotate()
        {
            lock (Mutex)
            {
                Console.Clear();
                Field.Put(Tetramino, false);
                Tetramino.Rotate();
                Field.Put(Tetramino, true);
                DisplayField();
            }
        }

        private Tetramino GenerateTetramino()
        {
            int x = Field.WIDTH / 2 - 1;
            int y = 0;

            switch (new Random().Next(1, 8))
            {
                case 1: return new I(x, y);
                case 2: return new J(x, y);
                case 3: return new L(x, y);
                case 4: return new S(x, y);
                case 5: return new T(x, y);
                case 6: return new Z(x, y);
                case 7: return new O(x, y);
                default: return new O(x, y);
            }
        }

        private Tetramino GetNextTetramino()
        {
            while (QueueTetramino.Count < 2)
            {
                QueueTetramino.Enqueue(GenerateTetramino());
            }
            Tetramino t = QueueTetramino.Dequeue();
            Field.SetNextTetramino(QueueTetramino.ToArray()[0]);
            return t;
        }

        public void DisplayField()
        {
            for (int i = 0; i < Field.HEIGHT; i++)
            {
                Console.Write("*");
                for (int j = 0; j < Field.WIDTH; j++)
                {
                    Console.Write(Field.Matrix[i, j] ? (char)9835 : ' ');
                }
                Console.Write("* ");
                if (i == 0)
                {
                    Console.Write("Next");
                }
                if (i > 1 && i < 6)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        Console.Write(Field.NextMatrix[i-2,j] ? (char)9835 : ' ');
                    }
                }

                if (i == 7)
                {
                    Console.Write(string.Format(" Score: {0}", Score));
                }
                Console.WriteLine();
            }
            Console.WriteLine("************");
        }

        public static GameManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GameManager();
                }
                return _instance;
            }
        }

        private Queue<Tetramino> QueueTetramino = new Queue<Tetramino>();

        private static GameManager _instance = null;
        private Timer _timer = new Timer(300);
        private object Mutex = new object();
    }
}
