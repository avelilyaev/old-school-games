// Tetris.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>

using namespace std;

enum TetraType { I, J, L, O, Z, S, T };
class Point { public: int X; int Y; Point(int x, int y) { X = x; Y = y; } };
const int WDTH = 10, HGHT = 20;
const int INTERVAL = 500;
static int Field[HGHT][WDTH];
static Point Tetramino[4] = { Point(0,0), Point(0,0), Point(0,0), Point(0,0) };

static int Coordinates[7][8] = { { 0, 0, 0, 1, 0, 2, 0, 3 },{ 1, 0, 1, 1, 1, 2, 0, 2 },{ 0, 0, 0, 1, 0, 2, 1, 2 },{ 0, 0, 1, 0, 0, 1, 1, 1 },{ 0, 0, 1, 0, 1, 1, 2, 1 },{ 1, 0, 2, 0, 0, 1, 1, 1 },{ 1, 1, 0, 0, 1, 0, 2, 0 } };
static int RotatingData[7][4][8] = { { { -1, 1, 0, 1, 1, 1, 2, 1 },{ 1, -1, 1, 0, 1, 1, 1, 2 },{ -1, 1, 0, 1, 1, 1, 2, 1 },{ 1, -1, 1, 0, 1, 1, 1, 2 } },{ { 1, 2, 0, 2, -1, 2, -1, 1 },{ -1, -2, -2, -2, -2, -1, -2, 0 },{ -1, 1, 0, 1, 1, 1, 1, 2 },{ 1, -1, 1, 0, 1, 1, 0, 1 } },{ { 0, 2, 0, 1, 1, 1, 2, 1 },{ 0, -2, 1, -2, 1, -1, 1, 0 },{ 0, 2, 1, 2, 2, 2, 2, 1 },{ 0, -2, 0, -1, 0, 0, 1, 0 } },{ { -1, -1, -1, -1, -1, -1, -1, -1 },{ -1, -1, -1, -1, -1, -1, -1, -1 },{ -1, -1, -1, -1, -1, -1, -1, -1 },{ -1, -1, -1, -1, -1, -1, -1, -1 } },{ { 1, 0, 1, 1, 0, 1, 0, 2 },{ -1, 0, 0, 0, 0, 1, 1, 1 },{ 1, 0, 1, 1, 0, 1, 0, 2 },{ -1, 0, 0, 0, 0, 1, 1, 1 } },{ { -1, 0, -1, 1, 0, 1, 0, 2 },{ 1, 0, 2, 0, 0, 1, 1, 1 },{ -1, 0, -1, 1, 0, 1, 0, 2 },{ 1, 0, 2, 0, 0, 1, 1, 1 } },{ { 0, 0, 1, -1, 1, 0, 1, 1 },{ 0, 0, -1, 1, 0, 1, 1, 1 },{ 0, 0, -1, -1, -1, 0, -1, 1 },{ 0, 0, -1, -1, 0, -1, 1, -1 } } };
static int LeftData[7][4][4] = { { { 0, 1, 2, 3 },{ 0, -1, -1, -1 },{ 0, 1, 2, 3 },{ 0, -1, -1, -1 } },	{ { 0, 1, -1, 3 },{ -1, -1, 2, 3 },{ -1, 1, 2, 3 },{ 0, -1, -1, 3 } },	{ { 0, 1, 2, -1 },{ 0, 1, -1, -1 },{ 0, -1, 2, 3 },{ 0, -1, -1, 3 } },	{ { 0, -1, 2, -1 },{ 0, -1, 2, -1 },{ 0, -1, 2, -1 },{ 0, -1, 2, -1 } },	{ { 0, -1, 2, -1 },{ 0, -1, 2, 3 },{ 0, -1, 2, -1 },{ 0, -1, 2, 3 } },	{ { 0, -1, 2, -1 },{ 0, 1, -1, 3 },{ 0, -1, 2, -1 },{ 0, 1, -1, 3 } },	{ { 0, 1, -1, -1 },{ 0, 1, -1, 3 },{ 0, 1, -1, -1 },{ -1, 1, 2, 3 } } };
static int RightData[7][4][4] = { { { 0, 1, 2, 3 },{ -1, -1, -1, 3 },{ 0, 1, 2, 3 },{ -1, -1, -1, 3 } },	{ { 0, 1, 2, -1 },{ 0, -1, -1, 3 },{ 0, -1, 2, 3 },{ -1, -1, 2, 3 } },	{ { 0, 1, -1, 3 },{ 0, -1, -1, 3 },{ -1, 1, 2, 3 },{ -1, -1, 2, 3 } },	{ { -1, 1, -1, 3 },{ -1, 1, -1, 3 },{ -1, 1, -1, 3 },{ -1, 1, -1, 3 } },	{ { -1, 1, -1, 3 },{ 0, 1, -1, 3 },{ -1, 1, -1, 3 },{ 0, 1, -1, 3 } },	{ { -1, 1, -1, 3 },{ 0, -1, 2, 3 },{ -1, 1, -1, 3 },{ 0, -1, 2, 3 } },	{ { 0, -1, -1, 3 },{ -1, 1, 2, 3 },{ 0, -1, -1, 3 },{ 0, 1, -1, 3 } } };
static int DownData[7][4][4] = { { { -1, -1, -1, 3 },{ 0, 1, 2, 3 },{ -1, -1, -1, 3 },{ 0, 1, 2, 3 } },	{ { -1, -1, 2, 3 },{ 0, 1, 2, -1 },{ 0, -1, -1, 3 },{ 0, 1, -1, 3 } },	{ { -1, -1, 2, 3 },{ 0, -1, 2, 3 },{ 0, -1, -1, 3 },{ 0, 1, 2, -1 } },	{ { -1, -1, 2, 3 },{ -1, -1, 2, 3 },{ -1, -1, 2, 3 },{ -1, -1, 2, 3 } },	{ { 0, -1, 2, 3 },{ -1, 1, -1, 3 },{ 0, -1, 2, 3 },{ -1, 1, -1, 3 } },	{ { -1, 1, 2, 3 },{ -1, 1, -1, 3 },{ -1, 1, 2, 3 },{ -1, 1, -1, 3 } },	{ { 0, -1, 1, 3 },{ 0, -1, -1, 3 },{ -1, 1, 2, 3 },{ 0, -1, -1, 3 } } };

static int _state = 0;
static int Score;
static TetraType CurrentType;

void Reset()
{
	Score = 0;

	for (int i = 0; i < HGHT; i++)
	{
		for (int j = 0; j < WDTH; j++)
		{
			Field[i][j] = 0;
		}
	}
}

void Display()
{
	system("cls");
	for (int row = 0; row < HGHT; row++)
	{
		printf("<");
		for (int col = 0; col < WDTH; col++)
		{
			printf(Field[row][col] == 1 ? "@" : " ");
		}
		printf(row == 0 ? "> score: " + Score : ">");
		printf("\n");
	}
	printf("||||||||||||\n");
}

void GameOver()
{
	//Timer.Stop();
	Score = 0;
	system("cls");
	printf("GAME OVER!!!");
}

void GenerateTetramino()
{
	_state = 0;
	CurrentType = (TetraType)(rand() % 7);
	for (int i = 0; i < 4; i++)
	{
		Tetramino[i].X = Coordinates[(int)CurrentType][i * 2] + WDTH / 2 - 1;
		Tetramino[i].Y = Coordinates[(int)CurrentType][i * 2 + 1];
	}

	for (int i = 0; i < 4; i++)
	{
		if (Field[Tetramino[i].Y][Tetramino[i].X] == 1)
		{
			GameOver();
		}
	}
}

void Put(bool value)
{
	for (int i = 0; i < 4; i++) Field[Tetramino[i].Y][Tetramino[i].X] = value ? 1 : 0;
}

void Move(int x, int y)
{
	Put(false);
	for (int i = 0; i < 4; i++)
	{
		Tetramino[i].X = Tetramino[i].X + x;
		Tetramino[i].Y = Tetramino[i].Y + y;
	}
	Put(true);
	Display();
}

bool CanMoveLeft()
{
	for (int i = 0; i < 4; i++)
	{
		int index = LeftData[(int)CurrentType][_state][i];
		if (index > -1 && (Tetramino[index].X == 0 || Field[Tetramino[index].Y][Tetramino[index].X - 1] == 1))
		{
			return false;
		}
	}
	return true;
}

bool CanMoveRight()
{
	for (int i = 0; i < 4; i++)
	{
		int index = RightData[(int)CurrentType][_state][i];
		if (index > -1 && (Tetramino[index].X == WDTH - 1 || Field[Tetramino[index].Y][Tetramino[index].X + 1] == 1))
		{
			return false;
		}
	}
	return true;
}

bool CanMoveDown()
{
	for (int i = 0; i < 4; i++)
	{
		int index = DownData[(int)CurrentType][_state][i];
		if (index > -1 && (Tetramino[index].Y >= HGHT - 1 || Field[Tetramino[index].Y + 1][Tetramino[index].X] == 1))
		{
			return false;
		}
	}
	return true;
}

int GetSum(int row)
{
	int sum = 0;
	for (int i = 0; i < WDTH; i++)
	{
		sum += Field[row][i];
	}
	return sum;
}

void ShiftField(int row)
{
	while (row > 0 && GetSum(row - 1) > 0)
	{
		for (int i = 0; i < WDTH; i++)
		{
			Field[row][i] = Field[row - 1][i];
			Field[row - 1][i] = 0;
		}
		row--;
	}
}

void CheckFullLines()
{
	if (!CanMoveDown())
	{
		for (int row = 0; row < HGHT; row++)
		{
			if (GetSum(row) == WDTH)
			{
				Score += 10;
				for (int col = 0; col < WDTH; col++)
				{
					Field[row][col] = 0;
				}
				ShiftField(row);
				Display();
			}
		}
	}
}

void SwitchState()
{
	_state = _state == 3 ? 0 : _state + 1;
}

bool Contains(Point point)
{
	for (int i = 0; i < 4; i++)
	{
		if (Tetramino[i].X == point.X && Tetramino[i].Y == point.Y)
		{
			return true;
		}
	}
	return false;
}

void Rotate()
{
	Point TestTetramino[] = { Point(0,0), Point(0,0), Point(0,0), Point(0,0) };

	Point p = Point(1, 3);

	int x = Tetramino[0].X;
	int y = Tetramino[0].Y;

	for (int i = 0; i < 4; i++)
	{
		TestTetramino[i].X = x + RotatingData[(int)CurrentType][_state][i * 2];
		TestTetramino[i].Y = y + RotatingData[(int)CurrentType][_state][i * 2 + 1];
	}

	if (CurrentType != TetraType::O)
	{
		bool canRotate = true;

		for (int i = 0; i < 4; i++)
		{
			if (TestTetramino[i].X < 0 || TestTetramino[i].X >= WDTH || TestTetramino[i].Y < 0 || TestTetramino[i].Y >= HGHT || (Field[TestTetramino[i].Y][TestTetramino[i].X] == 1 && !Contains(TestTetramino[i])))
			{
				canRotate = false;
			}
		}
		if (canRotate)
		{
			SwitchState();
			Put(false);
			for (int i = 0; i < 4; i++)	Tetramino[i] = TestTetramino[i];
			Put(true);
			Display();
		}
	}
}

void MakeMove()
{
	if (CanMoveDown())
	{
		Move(0, 1);
	}
	else
	{
		CheckFullLines();
		GenerateTetramino();
	}
}

int main()
{
	Reset();
	GenerateTetramino();
	Put(true);
	Display();

	int c;

	c = getchar();
	do
	{
		c = getchar();

		switch (c)
		{
		case 115:
		{
			MakeMove();
			break;
		}
		case 97:
		{
			if (CanMoveLeft()) Move(-1, 0);
			break;
		}
		case 100:
		{
			if (CanMoveRight()) Move(1, 0);
			break;
		}
		case 119:
		{
			Rotate();
			break;
		}
		}
	} while (true);

	return 0;
}
