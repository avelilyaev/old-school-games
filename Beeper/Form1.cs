﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Beeper
{
    public partial class Form1 : Form
    {
        public static int frequency = 37;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            while(frequency <= 32767)
            {
                //int f = new Random().Next(37, 32767);
                Console.Beep(frequency, frequency);
                frequency+=1;
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}
