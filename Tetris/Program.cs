﻿using System;
using System.Drawing;
using System.Timers;

namespace MiniTetris
{
    enum TetraType { I, J, L, O, Z, S, T }
    class Program
    {
        const int WDTH = 10, HGHT = 20;
        const int INTERVAL = 500;
        static int[,] Field;
        static Point[] Tetramino = new Point[4];
        static int[,,] LeftData = new int[,,] { { { 0, 1, 2, 3 }, { 0, -1, -1, -1 }, { 0, 1, 2, 3 }, { 0, -1, -1, -1 } }, { { 0, 1, -1, 3 }, { -1, -1, 2, 3 }, { -1, 1, 2, 3 }, { 0, -1, -1, 3 } }, { { 0, 1, 2, -1 }, { 0, 1, -1, -1 }, { 0, -1, 2, 3 }, { 0, -1, -1, 3 } }, { { 0, -1, 2, -1 }, { 0, -1, 2, -1 }, { 0, -1, 2, -1 }, { 0, -1, 2, -1 } }, { { 0, -1, 2, -1 }, { 0, -1, 2, 3 }, { 0, -1, 2, -1 }, { 0, -1, 2, 3 } }, { { 0, -1, 2, -1 }, { 0, 1, -1, 3 }, { 0, -1, 2, -1 }, { 0, 1, -1, 3 } }, { { 0, 1, -1, -1 }, { 0, 1, -1, 3 }, { 0, 1, -1, -1 }, { -1, 1, 2, 3 } } };
        static int[,,] RightData = new int[,,] { { { 0, 1, 2, 3 }, { -1, -1, -1, 3 }, { 0, 1, 2, 3 }, { -1, -1, -1, 3 } }, { { 0, 1, 2, -1 }, { 0, -1, -1, 3 }, { 0, -1, 2, 3 }, { -1, -1, 2, 3 } }, { { 0, 1, -1, 3 }, { 0, -1, -1, 3 }, { -1, 1, 2, 3 }, { -1, -1, 2, 3 } }, { { -1, 1, -1, 3 }, { -1, 1, -1, 3 }, { -1, 1, -1, 3 }, { -1, 1, -1, 3 } }, { { -1, 1, -1, 3 }, { 0, 1, -1, 3 }, { -1, 1, -1, 3 }, { 0, 1, -1, 3 } }, { { -1, 1, -1, 3 }, { 0, -1, 2, 3 }, { -1, 1, -1, 3 }, { 0, -1, 2, 3 } }, { { 0, -1, -1, 3 }, { -1, 1, 2, 3 }, { 0, -1, -1, 3 }, { 0, 1, -1, 3 } } };
        static int[,,] DownData = new int[,,] { { { -1, -1, -1, 3 }, { 0, 1, 2, 3 }, { -1, -1, -1, 3 }, { 0, 1, 2, 3 } }, { { -1, -1, 2, 3 }, { 0, 1, 2, -1 }, { 0, -1, -1, 3 }, { 0, 1, -1, 3 } }, { { -1, -1, 2, 3 }, { 0, -1, 2, 3 }, { 0, -1, -1, 3 }, { 0, 1, 2, -1 } }, { { -1, -1, 2, 3 }, { -1, -1, 2, 3 }, { -1, -1, 2, 3 }, { -1, -1, 2, 3 } }, { { 0, -1, 2, 3 }, { -1, 1, -1, 3 }, { 0, -1, 2, 3 }, { -1, 1, -1, 3 } }, { { -1, 1, 2, 3 }, { -1, 1, -1, 3 }, { -1, 1, 2, 3 }, { -1, 1, -1, 3 } }, { { 0, 1, -1, 3 }, { 0, -1, -1, 3 }, { -1, 1, 2, 3 }, { 0, -1, -1, 3 } } };
        static int[,] Coordinates = new int[,] { { 0, 0, 0, 1, 0, 2, 0, 3 }, { 1, 0, 1, 1, 1, 2, 0, 2 }, { 0, 0, 0, 1, 0, 2, 1, 2 }, { 0, 0, 1, 0, 0, 1, 1, 1 }, { 0, 0, 1, 0, 1, 1, 2, 1 }, { 1, 0, 2, 0, 0, 1, 1, 1 }, { 1, 1, 0, 0, 1, 0, 2, 0 } };
        static int[,,] RotatingData = new int[,,] { { { -1, 1, 0, 1, 1, 1, 2, 1 }, { 1, -1, 1, 0, 1, 1, 1, 2 }, { -1, 1, 0, 1, 1, 1, 2, 1 }, { 1, -1, 1, 0, 1, 1, 1, 2 } }, { { 1, 2, 0, 2, -1, 2, -1, 1 }, { -1, -2, -2, -2, -2, -1, -2, 0 }, { -1, 1, 0, 1, 1, 1, 1, 2 }, { 1, -1, 1, 0, 1, 1, 0, 1 } }, { { 0, 2, 0, 1, 1, 1, 2, 1 }, { 0, -2, 1, -2, 1, -1, 1, 0 }, { 0, 2, 1, 2, 2, 2, 2, 1 }, { 0, -2, 0, -1, 0, 0, 1, 0 } }, { { -1, -1, -1, -1, -1, -1, -1, -1 }, { -1, -1, -1, -1, -1, -1, -1, -1 }, { -1, -1, -1, -1, -1, -1, -1, -1 }, { -1, -1, -1, -1, -1, -1, -1, -1 } }, { { 1, 0, 1, 1, 0, 1, 0, 2 }, { -1, 0, 0, 0, 0, 1, 1, 1 }, { 1, 0, 1, 1, 0, 1, 0, 2 }, { -1, 0, 0, 0, 0, 1, 1, 1 } }, { { -1, 0, -1, 1, 0, 1, 0, 2 }, { 1, 0, 2, 0, 0, 1, 1, 1 }, { -1, 0, -1, 1, 0, 1, 0, 2 }, { 1, 0, 2, 0, 0, 1, 1, 1 } }, { { 0, 0, 1, -1, 1, 0, 1, 1 }, { 0, 0, -1, 1, 0, 1, 1, 1 }, { 0, 0, -1, -1, -1, 0, -1, 1 }, { 0, 0, -1, -1, 0, -1, 1, -1 } } };

        static Timer Timer = new Timer(INTERVAL);
        static object mutex = new object();

        static int Score;
        static TetraType CurrentType;

        static void Reset()
        {
            Score = 0;
            Field = new int[HGHT, WDTH];
        }

        static void Display()
        {
            Console.Clear();
            for (int row = 0; row < HGHT; row++)
            {
                Console.Write("<");
                for (int col = 0; col < WDTH; col++)
                {
                    Console.Write(Field[row, col] == 1 ? "@" : " ");
                }
                Console.WriteLine(row == 0 ? string.Format("> score: {0}", Score) : ">");
            }
            Console.WriteLine("||||||||||||");
        }

        static void GenerateTetramino()
        {
            _state = 0;
            CurrentType = (TetraType)new Random().Next(0, 7);
            for (int i = 0; i < 4; i++) Tetramino[i] = new Point(Coordinates[(int)CurrentType, i * 2] + WDTH / 2 - 1, Coordinates[(int)CurrentType, i * 2 + 1]);

            for (int i = 0; i < 4; i++)
            {
                if (Field[Tetramino[i].Y, Tetramino[i].X] == 1)
                {
                    GameOver();
                }
            }
        }

        static void GameOver()
        {
            Timer.Stop();
            Score = 0;
            Console.Clear();
            Console.WriteLine("GAME OVER!!!");
        }

        static void Put(bool value)
        {
            for (int i = 0; i < 4; i++) Field[Tetramino[i].Y, Tetramino[i].X] = value ? 1 : 0;
        }

        static void Move(int x, int y)
        {
            lock (mutex)
            {
                Put(false);
                for (int i = 0; i < 4; i++) Tetramino[i] = new Point(Tetramino[i].X + x, Tetramino[i].Y + y);
                Put(true);
                Display();
            }
        }

        static bool CanMoveLeft()
        {
            for (int i = 0; i < 4; i++)
            {
                int index = LeftData[(int)CurrentType, _state, i];
                if (index > -1 && (Tetramino[index].X == 0 || Field[Tetramino[index].Y, Tetramino[index].X - 1] == 1))
                {
                    return false;
                }
            }
            return true;
        }

        static bool CanMoveRight()
        {
            for (int i = 0; i < 4; i++)
            {
                int index = RightData[(int)CurrentType, _state, i];
                if (index > -1 && (Tetramino[index].X == WDTH - 1 || Field[Tetramino[index].Y, Tetramino[index].X + 1] == 1))
                {
                    return false;
                }
            }
            return true;
        }

        static bool CanMoveDown()
        {
            for (int i = 0; i < 4; i++)
            {
                int index = DownData[(int)CurrentType, _state, i];
                if (index > -1 && (Tetramino[index].Y >= HGHT - 1 || Field[Tetramino[index].Y + 1, Tetramino[index].X] == 1))
                {
                    return false;
                }
            }
            return true;
        }

        static void ShiftField(int row)
        {
            while (row > 0 && GetSum(row - 1) > 0)
            {
                for (int i = 0; i < WDTH; i++)
                {
                    Field[row, i] = Field[row - 1, i];
                    Field[row - 1, i] = 0;
                }
                row--;
            }
        }

        static int GetSum(int row)
        {
            int sum = 0;
            for (int i = 0; i < WDTH; i++)
            {
                sum += Field[row, i];
            }
            return sum;
        }

        static void CheckFullLines()
        {
            if (!CanMoveDown())
            {
                for (int row = 0; row < HGHT; row++)
                {
                    if (GetSum(row) == WDTH)
                    {
                        Score += 10;
                        for (int col = 0; col < WDTH; col++)
                        {
                            Field[row, col] = 0;
                        }
                        ShiftField(row);
                        Display();
                    }
                }
            }
        }

        static void Rotate()
        {
            Point[] TestTetramino = new Point[4];

            int x = Tetramino[0].X;
            int y = Tetramino[0].Y;

            for (int i = 0; i < 4; i++)
            {
                TestTetramino[i] = new Point(x + RotatingData[(int)CurrentType, _state, i * 2], y + RotatingData[(int)CurrentType, _state, i * 2 + 1]);
            }

            if (CurrentType != TetraType.O)
            {
                bool canRotate = true;

                for (int i = 0; i < 4; i++)
                {
                    if (TestTetramino[i].X < 0 || TestTetramino[i].X >= WDTH || TestTetramino[i].Y < 0 || TestTetramino[i].Y >= HGHT || (Field[TestTetramino[i].Y, TestTetramino[i].X] == 1 && !Contains(TestTetramino[i])))
                    {
                        canRotate = false;
                    }
                }
                if (canRotate)
                {
                    SwitchState();
                    Put(false);
                    Tetramino = TestTetramino;
                    Put(true);
                    Display();
                }
            }
        }

        private static bool Contains(Point point)
        {
            for (int i = 0; i < 4; i++)
            {
                if (Tetramino[i].X == point.X && Tetramino[i].Y == point.Y)
                {
                    return true;
                }
            }
            return false;
        }

        static void MakeMove()
        {
            if (CanMoveDown())
            {
                Move(0, 1);
            }
            else
            {
                CheckFullLines();
                GenerateTetramino();
            }
        }

        static void Main(string[] args)
        {
            Reset();
            GenerateTetramino();
            Put(true);
            Display();

            Timer.Elapsed += delegate
            {
                MakeMove();
            };
            Timer.Start();

            ConsoleKeyInfo kInfo;
            do
            {
                kInfo = Console.ReadKey();

                switch (kInfo.Key)
                {
                    case ConsoleKey.DownArrow:
                        {
                            MakeMove();
                            break;
                        }
                    case ConsoleKey.LeftArrow:
                        {
                            if (CanMoveLeft()) Move(-1, 0);
                            break;
                        }
                    case ConsoleKey.RightArrow:
                        {
                            if (CanMoveRight()) Move(1, 0);
                            break;
                        }
                    case ConsoleKey.UpArrow:
                        {
                            Rotate();
                            break;
                        }
                }
            }
            while (kInfo.Key != ConsoleKey.Escape);
        }

        public static void SwitchState()
        {
            _state = _state == 3 ? 0 : _state + 1;
        }

        private static int _state = 0;
    }
}
