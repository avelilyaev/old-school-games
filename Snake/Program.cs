﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Timers;

namespace Snake
{
    class Program
    {
        const int HEIGTH = 20;
        const int WIDTH = 10;
        static bool[,] Matrix = new bool[HEIGTH, WIDTH];
        static List<Point> Snake;
        static Point Eat = new Point(0, 0);
        static ConsoleKeyInfo Key;
        static Timer Timer = new Timer(300);
        static object Mutex = new object();
        static int Score = 0;

        static void Reset()
        {
            Snake = new List<Point>(new Point[] { new Point(4, 9), new Point(4, 10) });
            Score = 0;
        }

        static void GenerateEat()
        {
            do
            {
                Eat = new Point(new Random().Next(0, WIDTH - 1), new Random().Next(0, HEIGTH - 1));
            }
            while (Snake.Contains(Eat));
        }

        static void PrintSnake()
        {
            foreach (Point point in Snake)
            {
                Matrix[point.Y, point.X] = true;
            }

            Matrix[Eat.Y, Eat.X] = true;

            for (int i = 0; i < HEIGTH; i++)
            {
                Console.Write("*");
                for (int j = 0; j < WIDTH; j++)
                {
                    Console.Write(Matrix[i, j] ? (i == Eat.Y && j == Eat.X ? '@' : (char)9835) : ' ');
                }
                Console.WriteLine("*" + (i == 0 ? string.Format("Score: {0}", Score) : ""));
            }
            Console.WriteLine("************");
        }

        static void ClearMatrix()
        {
            Matrix = new bool[HEIGTH, WIDTH];
        }

        static bool InField(Point point)
        {
            return point.X >= 0 && point.X < WIDTH && point.Y >= 0 && point.Y < HEIGTH;
        }

        static void Move(ConsoleKey key)
        {
            lock (Mutex)
            {
                Console.Clear();
                ClearMatrix();

                Point newPoint = new Point(0, 0);
                if (key == ConsoleKey.LeftArrow) newPoint = new Point(Snake.Last().X - 1, Snake.Last().Y);
                else if (key == ConsoleKey.RightArrow) newPoint = new Point(Snake.Last().X + 1, Snake.Last().Y);
                else if (key == ConsoleKey.UpArrow) newPoint = new Point(Snake.Last().X, Snake.Last().Y - 1);
                else if (key == ConsoleKey.DownArrow) newPoint = new Point(Snake.Last().X, Snake.Last().Y + 1);

                if (!InField(newPoint) || Snake.Contains(newPoint) && Snake.IndexOf(newPoint) != Snake.Count - 2)
                {
                    Console.Clear();
                    Console.WriteLine("Game over");
                    Console.ReadKey();
                    Reset();
                }
                else
                {
                    if (!Snake.Contains(newPoint))
                    {
                        if (newPoint == Eat)
                        {
                            GenerateEat();
                            Score++;
                        }
                        else
                        {
                            Snake.Remove(Snake.First());
                        }
                        Snake.Add(newPoint);
                    }
                    PrintSnake();
                }
            }
        }

        static void MakeMove(object sender, ElapsedEventArgs e)
        {
            if (Key.Key == ConsoleKey.DownArrow || Key.Key == ConsoleKey.UpArrow || Key.Key == ConsoleKey.LeftArrow || Key.Key == ConsoleKey.RightArrow)
            {
                Move(Key.Key);
            }
        }

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Reset();
            Timer.Elapsed += MakeMove;
            Timer.Start();
            GenerateEat();
            PrintSnake();
            do
            {
                Key = Console.ReadKey();
                MakeMove(null, null);
            }
            while (Key.Key != ConsoleKey.Escape);
        }
    }
}
